## INTRODUCTION

The Content Type Report module provides a simple report of the number of nodes
 for each content type in your Drupal site.

## FEATURES

- Filter: It allows filtering by published & unpublished nodes for the count.
- Simple Interface: User-friendly interface designed for quick 
   access and easy understanding.
- Quick Insights: Easily identify which content types are most frequently used. 
   Also includes unused content types with a count of 0.
- Easy Access: It offers a link to the Content 
   Overview Page for each content type.

The primary use case for this module is:

- #1 Audit: Helps understand the distribution 
   and status of content across different types.
- #2 Cleanup: Identify unused content types to cleanup 
    and improve the content editing experience.
- #3 Migration: Facilitate migration efforts 
  by providing a clear overview of content type usage.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## USAGE

- Navigate to Reports -> Content Type Audit using the Admin Toolbar.
- Alternatively, you can also use this link: `/admin/reports/content-type.`

## MAINTAINERS

Current maintainers for Drupal 8, 9, 10 & 11:

- Chirag K Parikh - https://www.drupal.org/u/chiragp185
