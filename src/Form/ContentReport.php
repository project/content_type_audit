<?php

namespace Drupal\content_type_audit\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Generates a Report of all Content Types.
 */
class ContentReport extends FormBase {
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Contains a master list of all content types.
   *
   * @var array
   */
  public $contentTypes;

  /**
   * {@inheritDoc}
   */
  protected $connection;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = Database::getConnection();
    $this->contentTypes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('entity_type.manager')
      );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'content_type_audit';
  }

  /**
   * Builds the Status dropdown Form & Table.
   */
  public function buildForm($form, FormStateInterface $form_state) {
    $status = $form_state->get('status') ?? 'any';
    $form['status'] = [
      '#type' => 'select',
      '#title' => $this->t('Published Status'),
      '#prefix' => '<h1 class="align-text-center">Content Type Report</h1>',
      '#options' => [
        'any' => $this->t('Any'),
        'published' => $this->t('Published'),
        'unpublished' => $this->t('Unpublished'),
      ],
      '#ajax' => [
        'callback' => [$this, 'updateTable'],
        'wrapper' => 'table-div',
      ],
    ];

    $form['table'] = [
      '#type' => 'markup',
      '#markup' => $this->getTable($status),
    ];
    $form['#attached']['library'][] = 'content_type_audit/content_type_audit.content_report';
    return $form;
  }

  /**
   * AJAX Callback that fetched the table values.
   */
  public function updateTable(&$form, FormStateInterface $form_state) {
    $status = $form_state->getValue('status');
    return ['#markup' => $this->getTable($status)];
  }

  /**
   * Queries the database for the nodes based on the status value.
   *
   * @return string
   *   The string contains the HTML that renders the table.
   */
  public function getTable($status) {
    $query = $this->connection->select('node_field_data', 'nfd');
    $query->addField('nfd', 'type', 'node_field_data_type');
    $query->addExpression('COUNT(DISTINCT nfd.nid)', 'nid');
    $query->addExpression('MIN(nfd.nid)', 'nid_1');
    // Add a condition to the WHERE clause for Published/Unpublished nodes.
    if ($status !== 'any') {
      if ($status == 'published') {
        $query->condition('nfd.status', '1');
      }
      elseif ($status == 'unpublished') {
        $query->condition('nfd.status', '0');
      }
    }
    $query->groupBy('nfd.type');
    $results = $query->execute()->fetchAll();
    $data = array_fill_keys(array_keys($this->contentTypes), '0');
    foreach ($results as $result) {
      $data[$result->node_field_data_type] = $result->nid;
    }
    $prefix = '<table class="content_type_list" id="table-div"><tr>
                   <th class="heading-content-type">Content Type</th>
                   <th class="count">Number of Pages</th>
                   <th class="align-text-center">Link</th></tr>';
    $markup = '';
    foreach ($data as $content_type => $value) {
      $tr = '<td>' . $this->contentTypes[$content_type]->label() . '</td><td class="count">' . $value . '</td>';
      if ($value != '0') {
        $link = '/admin/content/?type=' . $content_type;
        if ($status == 'published') {
          $link .= '&status=1';
        }
        elseif ($status == 'unpublished') {
          $link .= '&status=2';
        }
        else {
          $link .= '&status=All';
        }
        $tr .= '<td class="align-text-center"><a target="_blank" href="' . $link . '">View All Pages</a></td>';
      }
      $markup .= '<tr class="table-row">' . $tr . '</tr>';
    }
    $total = array_sum($data);
    $markup .= '<tr class="table-row"><td><strong>Total</strong></td><td class="count">' . $total . '</td></tr>';
    $markup = $prefix . $markup . '</table>';
    return $markup;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Handled by AJAX.
  }

}
